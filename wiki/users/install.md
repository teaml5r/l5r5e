# Installation

## System Installation
### With Search (recommended)
1. Open FoundryVTT.
2. In the `Game Systems` tab, clic `Install system`.
3. Search `L5R`, on the line `Legend of the Five Rings (5th Edition)`, clic `Install`.

### With the manifest
1. Open FoundryVTT.
2. In the `Game Systems` tab, clic `Install system`.
3. Copy this link and use it in the `Manifest URL`, then clic `Install`.
> https://gitlab.com/teaml5r/l5r5e/-/raw/master/system/system.json


## Modules
L5R do not required a lot of module, i highly encourage you to start with a small number of it.

Some modules require others library/module, you need to install them to allow the primary module work.
Nothing fancy, just accept when FoundryVTT prompt you to download or activate the dependencies.


### Some recommended modules
| Module name                                                                        | Notes                                                                                                                                                                       |
|------------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| [Babele](https://foundryvtt.com/packages/babele)                                   | Required for non english compendium translation                                                                                                                             |
| [Ownership Viewer](https://foundryvtt.com/packages/permission_viewer)              | Lets you see instantly who has permissions to see what item                                                                                                                 |
| [Dice So Nice!](https://foundryvtt.com/packages/dice-so-nice)                      | Add 3D dices that bounce on the screen when you roll dice                                                                                                                   |
| [Small Legend of the 5 Rings Tools](https://foundryvtt.com/packages/l5r-dragruler) | Series of tools for L5R                                                                                                                                                     |
| [Search Anywhere](https://foundryvtt.com/packages/searchanywhere)                  | Don't spent too much time searching the right technique                                                                                                                     |
| [FXMaster](https://foundryvtt.com/packages/fxmaster)                               | More effects                                                                                                                                                                |
| [Scene Clicker](https://foundryvtt.com/packages/scene-clicker)                     | Clicking on a Scene or a Scene Link will now "view" the Scene instead of rendering the Scene Config Sheet                                                                   |
| [Universal Battlemap Importer](https://foundryvtt.com/packages/dd-import)          | Allows Importing [DungeonDraft](https://dungeondraft.net/), [DungeonFog](https://www.dungeonfog.com/) or [Arkenforge](https://arkenforge.com/) export files into FoundryVTT |
| [Compendium Folders](https://foundryvtt.com/packages/compendium-folders)           | Add folders to compendiums                                                                                                                                                  |
| [Chat Images](https://foundryvtt.com/packages/chat-images)                         | Lets you drag images into the chat, one of the quicker ways to do 'he looks like this'                                                                                      |
| [Combat Utility Belt](https://foundryvtt.com/packages/combat-utility-belt)         | A totally over-engineered but helpful app that will, among other things, let you set up custom statuses                                                                     |
| [Timer](https://foundryvtt.com/packages/timer)                                     | A simple timer, useful to stress a little your players                                                                                                                      |


### Map module
The official 5e Rokugan map is publish under the module section in FoundryVTT.
- [L5R5e - Rokugan map for Legend of the Five Rings (5th edition)](https://foundryvtt.com/packages/l5r5e-map)


## Worlds
We have published the official free content in form of worlds ready to play :
- [L5R5E - Cresting Waves](https://foundryvtt.com/packages/l5r5e-world-waves)
- [L5R5E - In the Palace of the Emerald Champion](https://foundryvtt.com/packages/l5r5e-world-palace)
- [L5R5E - The Highwayman](https://foundryvtt.com/packages/l5r5e-world-highwayman)
- [L5R5E - The Scroll or the Blade](https://foundryvtt.com/packages/l5r5e-world-scroll)
- [L5R5E - The Knotted Tails](https://foundryvtt.com/packages/l5r5e-world-tails)
- [L5R5E - Wedding at Kyotei castle](https://foundryvtt.com/packages/l5r_mariage)
