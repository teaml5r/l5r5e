# L5R5e System Wiki

## For users
- [Installation and modules](users/install.md)
- [Updating - Bests practices](users/updating.md)
- [Basic usage and filling sheets](users/basic-usage.md)
- [Dice Rolling](users/dice.md)
- [Symbols replacement list](users/symbols.md)
- [Advanced : Techniques skill and difficulty syntaxe](users/techniques-syntaxe.md)
- [Advanced : Custom Compendiums](users/custom-compendiums.md)
- [Advanced : Using CUB for modifiers](users/cub-modifiers.md)

## For developers
- [System helping (Contribute)](dev/system-helping.md)
- [Snippets](dev/snippets.md)
- [Sockets API](dev/sockets.md)
- [DicePicker (DP)](dev/dicepicker.md)
- [Roll n Keep (RnK)](dev/rnk.md)
- [Roll](dev/roll.md)
- [Storage](dev/storage.md)
